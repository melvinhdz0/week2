const labelWeather = document.getElementsByClassName("resultWeather");
const imgWeather = document.getElementsByClassName("imgWeather");
const searchBar = document.getElementById("txtSearchBar");
const divSearchBox = document.getElementById("searhBox");
const lbnameCity = document.getElementById("lbNameCity");
const localValues = window.localStorage;
//Get data of weather
async function WeatherLocationInitial(locationWoeid) {
  const requestOptions = {
    method: "GET",
    redirect: "follow",
  };

  const response = await fetch(
    `https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/${locationWoeid}/`,
    requestOptions
  );

  if (response.ok) {
    const dataFetch = await response.json();
    return dataFetch;
  } else {
    return null;
  }
}
//display data of weather
async function displayWeather(woeid) {
  if (woeid) {
    const result = await WeatherLocationInitial(woeid);
    let maxTemp = [];
    let minTemp = [];
    let temp;
    lbnameCity.innerHTML = result.title;
    for (i = 0; i < 5; i++) {
      temp = result.consolidated_weather[i].max_temp;
      maxTemp.push(temp.toString().substring(0, 5));
      temp = result.consolidated_weather[i].min_temp;
      minTemp.push(temp.toString().substring(0, 5));

      imgWeather[i].setAttribute(
        "src",
        `https://www.metaweather.com/static/img/weather/png/64/${result.consolidated_weather[i].weather_state_abbr}.png`
      );

      labelWeather[i].innerHTML =
        result.consolidated_weather[i].applicable_date +
        "\n" +
        result.consolidated_weather[i].weather_state_name +
        "\nMax:" +
        maxTemp[i] +
        " Min: " +
        minTemp[i];
    }
  } else {
    const result = await WeatherLocationInitial("2487956");
    let maxTemp = [];
    let minTemp = [];
    let temp;
    lbnameCity.innerHTML = result.title;
    for (i = 0; i < 5; i++) {
      temp = result.consolidated_weather[i].max_temp;
      maxTemp.push(temp.toString().substring(0, 5));
      temp = result.consolidated_weather[i].min_temp;
      minTemp.push(temp.toString().substring(0, 5));

      imgWeather[i].setAttribute(
        "src",
        `https://www.metaweather.com/static/img/weather/png/64/${result.consolidated_weather[i].weather_state_abbr}.png`
      );
      labelWeather[i].innerHTML =
        result.consolidated_weather[i].applicable_date +
        "\n" +
        result.consolidated_weather[i].weather_state_name +
        "\nMax:" +
        maxTemp[i] +
        " Min: " +
        minTemp[i];
    }
  }
}

//function to search Cities
async function searchCity(searchText) {
  let arrayCities = [];
  //solicitud Get para obtener ciudades
  const requestOptions = {
    method: "GET",
    redirect: "follow",
  };

  const response = await fetch(
    `https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/search/?query=${searchText}`,
    requestOptions
  );
  if (response.ok) {
    const dataFetch = await response.json();
    for (let i = 0; i < dataFetch.length; i++) {
      arrayCities.push({
        titleFetch: dataFetch[i].title.toString(),
        woeidFetch: dataFetch[i].woeid,
      });
    }
    return arrayCities;
  } else {
    return arrayCities;
  }
}

function closeAutoComplete(elementList) {
  let list = document.getElementsByClassName("autocomplete-items");

  for (i = 0; i < list.length; i++) {
    if (elementList != list[i] && elementList != searchBar) {
      list[i].parentNode.removeChild(list[i]);
    }
  }
}

//funcion debounce
const debounce = (fn, delay) => {
  let timer;
  return function () {
    clearTimeout(timer);
    timer = setTimeout(() => {
      fn();
    }, delay);
  };
};

//busqueda y auto complete
let searchVal = "";
let searchValue = async () => {
  let cities = await searchCity(searchBar.value);
  if (cities.length > 0) {
    //crear auto complete

    closeAutoComplete();

    let inputbtn,
      divItems,
      completeBar = document.createElement("div");

    completeBar.setAttribute("id", searchBar.id + "autocomplete-list");
    completeBar.setAttribute("class", "autocomplete-items");
    searchBar.parentNode.appendChild(completeBar);

    for (i = 0; i < cities.length; i++) {
      if (
        cities[i].titleFetch
          .substring(0, searchBar.value.length)
          .toUpperCase() == searchBar.value.toUpperCase()
      ) {
        divItems = document.createElement("div");
        divItems.innerHTML =
          "<strong>" +
          cities[i].titleFetch.substring(0, searchBar.value.length) +
          "</strong>";
        divItems.innerHTML += cities[i].titleFetch.substring(
          searchBar.value.length
        );
        divItems.innerHTML +=
          "<input type = 'hidden' value = '" +
          cities[i].titleFetch.toString() +
          "'/>";
        divItems.innerHTML +=
          "<input type = 'hidden' value = '" + cities[i].woeidFetch + "'/>";
        divItems.addEventListener("click", function (e) {
          searchBar.value = this.getElementsByTagName("input")[0].value;
          displayWeather(this.getElementsByTagName("input")[1].value);
          localValues.setItem('city',this.getElementsByTagName("input")[1].value);
          closeAutoComplete();
        });
        completeBar.appendChild(divItems);
      }
    }
  }
};
//set debounce
searchValue = debounce(searchValue, 500);
//values on start
displayWeather(localValues.getItem('city'));
//search action
searchBar.addEventListener("input", searchValue);
//close autocomplete on click
document.addEventListener("click", ()=>{
  closeAutoComplete();
})